import datetime
import numpy as np
import pandas as pd

from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.layers import Dense, Dropout, LSTM
from tensorflow.python.keras.models import Sequential
from tensorflow.keras.callbacks import ModelCheckpoint


class NetworkModel:

    def __init__(self, dataFrame):
        self.x_train = []
        self.y_train = []
        self.scaler = MinMaxScaler(feature_range=(0, 1))
        self.scaledData = self.scaler.fit_transform(dataFrame.values.reshape(-1, 1))
        self.predictionTicks = 0
        self.model = Sequential()
        self.checkpoint = ModelCheckpoint(filepath="best_model.hdf5", monitor='val_loss',
                                          save_best_only=True, mode="min")

    """
    prepare data for training Network Model
    """

    def prepareData(self):
        self.predictionTicks = 90
        for x in range(self.predictionTicks, len(self.scaledData)):
            self.x_train.append(self.scaledData[x - self.predictionTicks:x, 0])
            self.y_train.append(self.scaledData[x, 0])
        self.x_train = np.array(self.x_train)
        self.y_train = np.array(self.y_train)
        self.x_train = np.reshape(self.x_train, (self.x_train.shape[0], self.x_train.shape[1], 1))

    """
    create Network Model
    """

    def createModel(self):
        self.model.add(LSTM(units=self.x_train.shape[1], return_sequences=True, input_shape=(self.x_train.shape[1], 1)))
        self.model.add(Dropout(0.3))
        self.model.add(LSTM(units=int(self.x_train.shape[1] * 2 / 3), return_sequences=True))
        self.model.add(Dropout(0.3))
        self.model.add(LSTM(units=int(self.x_train.shape[1] / 3)))
        self.model.add(Dropout(0.3))
        self.model.add(Dense(units=10))

        self.model.compile(optimizer='adam', loss='mean_squared_error')

    """
    train Network Model
    """

    def trainModel(self, epoch):
        self.model.fit(self.x_train, self.y_train, epochs=epoch,
                       validation_split=0.2, callbacks=[self.checkpoint])
        self.model.summary()
        self.loadBestModel()

    """
    load best model to Network Model
    """

    def loadBestModel(self):
        self.model = Sequential()
        self.createModel()
        self.model.load_weights("best_model.hdf5")

    """
    get predicted Prices 
    """

    def getPredictedPrices(self):
        predictedPrices = self.scaler.inverse_transform(self.model.predict(self.x_train))
        return predictedPrices[:, 0]

    """
    predict the next value based on Network Model
    """

    def predictPrice(self, dataFrame):
        self.scaledData = self.scaler.fit_transform(dataFrame.values.reshape(-1, 1))
        realData = [self.scaledData[len(self.scaledData) - self.predictionTicks: len(self.scaledData + 1), 0]]
        realData = np.array(realData)
        realData = np.reshape(realData, (realData.shape[0], realData.shape[1], 1))
        prediction = self.scaler.inverse_transform(self.model.predict(realData))
        return prediction

    """
    add Future Days to Predicted Price
    """

    def addFutureDaysToPredictPrice(self, dataFrame, numberOfDays, prediction):
        table = pd.DataFrame({'Time': dataFrame[0],
                              'Close': dataFrame[1]})
        for number in range(numberOfDays):
            newDay = dataFrame[0].at[len(dataFrame[0]) - 1] + datetime.timedelta(days=1 + number)
            predictedPrice = prediction[0, number]
            pom = pd.DataFrame({'Time': [newDay], 'Close': [predictedPrice]})
            table = table.append(pom, ignore_index=True, sort=False)
        table = table[len(dataFrame[0]):len(table)]
        return [table['Time'], table['Close']]
