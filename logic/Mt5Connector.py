import MetaTrader5 as mt5
import pandas as pd
from datetime import datetime, timedelta
import pytz
from ui.BasicPopup import BasicPopup


class Mt5Connector:
    """
    connect to MetaTrader 5
    """

    def __init__(self):
        if not mt5.initialize():
            print("initialize() failed")
            BasicPopup.createPopup("Błąd połączenia z MetaTrader5")
            quit()
        else:
            print("connected to mt5")

    """
    get Ticks from MetaTrader5
    """

    def getTicksRange(self, symbol, startData, endData):
        if symbol == "":
            raise Exception("brak symbolu")
        interval = self.createInterval(startData, endData)
        data = mt5.copy_rates_range(symbol, interval, startData, endData)
        if data is None:
            raise Exception("brak danych")
        return data

    """
    get Ticks from startData to endData of EURCAD
    """

    def getTicksRangeForSI(self, startData, endData):
        data = mt5.copy_rates_range("EURCAD.", mt5.TIMEFRAME_D1, startData, endData)
        if data is None:
            raise Exception("brak danych")
        return data

    """
    get defined number of ticks from MetaTrader5 
    """

    def getDataForSI(self, numberOfDays):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(days=numberOfDays)
        d2 = datetime.now(timezone)
        mt5Data = self.getTicksRangeForSI(d1, d2)
        return mt5Data

    """
    get defined number of ticks from MetaTrader5 
    """

    def getData(self, symbol, startDate, endDate):
        d1 = startDate.toPyDateTime()
        d2 = endDate.toPyDateTime()
        mt5Data = self.getTicksRange(symbol, d1, d2)
        return mt5Data

    """
    selects an appropriate time interval for a given DateTime
    """

    def createInterval(self, startDate, endDate):
        minutes_diff = (endDate - startDate).total_seconds() / 60.0
        if minutes_diff <= 0:
            raise Exception("zły przedział")
        if minutes_diff > 1000000:
            return mt5.TIMEFRAME_MN1
        if minutes_diff > 100000:
            return mt5.TIMEFRAME_W1
        if minutes_diff > 10000:
            return mt5.TIMEFRAME_D1
        if minutes_diff > 1000:
            return mt5.TIMEFRAME_H1
        if minutes_diff < 10:
            raise Exception("podaj przedział większy bądź równy 10 min")
        return mt5.TIMEFRAME_M5

    """
    create Data Frame 
    """

    def createDataFrame(self, dataFrame, typeOfData):
        if dataFrame is None or len(dataFrame) == 0:
            raise Exception("dataFrame jest pusty")
        if typeOfData is None:
            raise Exception("typ danych nie został wybrany")
        data = pd.DataFrame(dataFrame)
        data.columns = 'Time', 'Open', 'High', 'Low', 'Close', 'Tick', 'Vol', 'Spread'
        data['Time'] = pd.to_datetime(data['Time'], unit='s')
        return [data['Time'], data[typeOfData]]
