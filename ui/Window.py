from PyQt5 import QtWidgets, QtCore, QtGui

from logic.NetworkModel import NetworkModel
from logic.Mt5Connector import Mt5Connector

from ui.Canvas import Canvas
from ui.Ui_Window import Ui_Window
from ui.BasicPopup import BasicPopup


class Window(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()
        self.ui = Ui_Window()
        self.mainWindow = QtWidgets.QMainWindow()
        self.ui.setupUi(self.mainWindow)
        self.mainWindow.show()
        self.isMaximized = False
        self.dataFrame = []
        self.mt5 = Mt5Connector()
        # create Canvas do Window2
        self.ui.plotWidget = Canvas(self.ui.plotFrame)
        self.ui.plotWidget.show()
        self.ui.verticalLayout_5.insertWidget(1, self.ui.plotWidget)
        self.ui.verticalLayout_5.setStretch(1, 10)
        # connect Buttons
        self.ui.plotButton.clicked.connect(lambda: self.createPlot())
        self.ui.aiButton.clicked.connect(lambda: self.createPredictionPlot())
        self.ui.exitButton.clicked.connect(lambda: self.closeApp())
        self.ui.minimizeButton.clicked.connect(lambda: self.minimizeApp())
        self.ui.maximizeButton.clicked.connect(lambda: self.maximizeApp())
        self.ui.backButton.clicked.connect(lambda: self.backPage())
        # set position
        self.dragPos = QtCore.QPoint()

        def mousePressEvent(event: QtGui.QMouseEvent):
            self.dragPos = event.globalPos()

        def moveWindow(event: QtGui.QMouseEvent):
            if event.buttons() == QtCore.Qt.LeftButton:
                self.mainWindow.move(self.mainWindow.pos() + event.globalPos() - self.dragPos)
                self.dragPos = event.globalPos()
                event.accept()

        self.ui.titleBar.mouseMoveEvent = moveWindow
        self.ui.titleBar.mousePressEvent = mousePressEvent

    def backPage(self):
        self.ui.stackedWidget.setCurrentIndex(0)

    def maximizeApp(self):
        if not self.isMaximized:
            self.isMaximized = True
            self.mainWindow.showMaximized()
            self.ui.maximizeButton.setToolTip("Restore")
            self.ui.centralwidget.setStyleSheet("background-color: "
                                                "qlineargradient(spread:pad, x1:0.96, y1:0.94318, x2:0.159045, y2:0.108"
                                                ", stop:0.025 rgba(0, 0, 0, 255), stop:0.9882 rgba(49, 86, 56, 255));")
        else:
            self.isMaximized = False
            self.mainWindow.showNormal()
            self.ui.maximizeButton.setToolTip("Maximize")
            self.ui.centralwidget.setStyleSheet("background-color:"
                                                "qlineargradient(spread:pad, x1:0.96, y1:0.94318, x2:0.159045, y2:0.108"
                                                ", stop:0.025 rgba(0, 0, 0, 255), stop:0.9882 rgba(49, 86, 56, 255));\n"
                                                "border-radius: 10px;")

    def minimizeApp(self):
        self.mainWindow.showMinimized()

    def closeApp(self):
        self.mainWindow.close()

    def checkRadioButton(self):
        if self.ui.openButton.isChecked():
            return "Open"
        if self.ui.closeButton.isChecked():
            return "Close"
        if self.ui.lowButton.isChecked():
            return "Low"
        if self.ui.highButton.isChecked():
            return "High"

    def createPlot(self):
        try:
            self.dataFrame = self.mt5.createDataFrame(
                self.mt5.getData(self.ui.symbolTextEdit.toPlainText(), self.ui.startDateTimeEdit.dateTime(),
                                 self.ui.endDateTimeEdit.dateTime()), self.checkRadioButton())
            self.ui.plotWidget.drawPlot(self.dataFrame)
            self.ui.stackedWidget.setCurrentIndex(1)
        except Exception as ex:
            BasicPopup.createPopup(str(ex))

    def createPredictionPlot(self):
        try:
            self.dataFrame = self.mt5.createDataFrame(self.mt5.getDataForSI(1000), "Close")
            self.ui.plotWidget.drawPlot(self.dataFrame)
            nm = NetworkModel(self.dataFrame[1])
            nm.prepareData()
            nm.createModel()
            nm.trainModel(200)
            predictedPrice = nm.getPredictedPrices()
            self.ui.plotWidget.addPredictionPrice([self.dataFrame[0][90:], predictedPrice], 'y')
            prediction = nm.predictPrice(self.dataFrame[1])
            self.ui.plotWidget.addPredictionPrice(
                nm.addFutureDaysToPredictPrice(self.dataFrame, prediction.shape[1], prediction), 'g')
            self.ui.stackedWidget.setCurrentIndex(1)
        except Exception as ex:
            BasicPopup.createPopup("błąd tworzenia sztucznej inteligencji")
