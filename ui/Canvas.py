from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure


class Canvas(FigureCanvas):
    """
    create Object Canvas which holds plot
    """

    def __init__(self, parent=None, width=6.4, height=4.8, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot()
        super(Canvas, self).__init__(self.fig)
        self.fig.tight_layout()

    """
    create Plot with blue line from data obtained from MetaTrader5
    """

    def drawPlot(self, dataFrame):
        self.axes.clear()
        self.axes.plot(dataFrame[0], dataFrame[1], 'b')
        self.draw()

    """
    add to existing Plot prediction data
    """

    def addPredictionPrice(self, dataFrame, color):
        self.axes.plot(dataFrame[0], dataFrame[1], color)
        self.draw()
