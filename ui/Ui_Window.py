# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'New_Ui_Window2.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Window(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(910, 630)
        MainWindow.setStyleSheet("")
        MainWindow.setWindowTitle("Grzegorz Kurenda")
        MainWindow.setWindowIcon(QtGui.QIcon('assets/images/Graph-03-24.png'))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        MainWindow.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        MainWindow.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.centralwidget.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.96, y1:0.943182, "
                                         "x2:0.159045, y2:0.108, stop:0.025 rgba(0, 0, 0, 255), stop:0.9882 rgba(49, "
                                         "86, 56, 255)); border-radius: 10px;")
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setContentsMargins(10, 0, 10, 5)
        self.verticalLayout.setSpacing(5)
        self.verticalLayout.setObjectName("verticalLayout")
        self.titleBar = QtWidgets.QFrame(self.centralwidget)
        self.titleBar.setEnabled(True)
        self.titleBar.setMinimumSize(QtCore.QSize(20, 30))
        self.titleBar.setMaximumSize(QtCore.QSize(16777215, 40))
        self.titleBar.setStyleSheet("background-color: none;")
        self.titleBar.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.titleBar.setFrameShadow(QtWidgets.QFrame.Raised)
        self.titleBar.setObjectName("titleBar")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.titleBar)
        self.horizontalLayout.setContentsMargins(0, 5, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.titleFrame = QtWidgets.QFrame(self.titleBar)
        self.titleFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.titleFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.titleFrame.setObjectName("titleFrame")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.titleFrame)
        self.verticalLayout_2.setContentsMargins(-1, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.titleLabel = QtWidgets.QLabel(self.titleFrame)
        font = QtGui.QFont()
        font.setFamily("MS Reference Sans Serif")
        font.setPointSize(14)
        self.titleLabel.setFont(font)
        self.titleLabel.setStyleSheet("color: white;")
        self.titleLabel.setText("Aplikacja do pozyskiwania danych handlowych z MetaTrader5")
        self.titleLabel.setObjectName("titleLabel")
        self.verticalLayout_2.addWidget(self.titleLabel)
        self.horizontalLayout.addWidget(self.titleFrame)
        self.buttonsFrame = QtWidgets.QFrame(self.titleBar)
        self.buttonsFrame.setMinimumSize(QtCore.QSize(100, 30))
        self.buttonsFrame.setMaximumSize(QtCore.QSize(100, 30))
        self.buttonsFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.buttonsFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.buttonsFrame.setObjectName("buttonsFrame")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.buttonsFrame)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.minimizeButton = QtWidgets.QPushButton(self.buttonsFrame)
        self.minimizeButton.setMinimumSize(QtCore.QSize(16, 16))
        self.minimizeButton.setMaximumSize(QtCore.QSize(16, 16))
        self.minimizeButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.minimizeButton.setStatusTip("")
        self.minimizeButton.setWhatsThis("")
        self.minimizeButton.setStyleSheet("QPushButton {\n"
"border: none;\n"
"border-radius: 8px;\n"
"background-color: rgb(20,255,12);\n"
"}\n"
"QPushButton:hover {\n"
"background-color: rgb(20,255,12,150);\n"
"}")
        self.minimizeButton.setText("")
        self.minimizeButton.setObjectName("minimizeButton")
        self.horizontalLayout_2.addWidget(self.minimizeButton)
        self.maximizeButton = QtWidgets.QPushButton(self.buttonsFrame)
        self.maximizeButton.setMinimumSize(QtCore.QSize(16, 16))
        self.maximizeButton.setMaximumSize(QtCore.QSize(16, 16))
        self.maximizeButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.maximizeButton.setStyleSheet("QPushButton {\n"
"border: none;\n"
"border-radius: 8px;\n"
"background-color: rgb(255, 214, 48);\n"
"}\n"
"QPushButton:hover {\n"
"background-color: rgb(255, 214, 48,150);\n"
"}")
        self.maximizeButton.setText("")
        self.maximizeButton.setObjectName("maximizeButton")
        self.horizontalLayout_2.addWidget(self.maximizeButton)
        self.exitButton = QtWidgets.QPushButton(self.buttonsFrame)
        self.exitButton.setMinimumSize(QtCore.QSize(16, 16))
        self.exitButton.setMaximumSize(QtCore.QSize(16, 16))
        self.exitButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.exitButton.setStyleSheet("QPushButton {\n"
"border: none;\n"
"border-radius: 8px;\n"
"background-color: rgb(255,5,12);\n"
"}\n"
"QPushButton:hover {\n"
"background-color: rgb(255,5,12,150);\n"
"}")
        self.exitButton.setText("")
        self.exitButton.setObjectName("exitButton")
        self.horizontalLayout_2.addWidget(self.exitButton)
        self.horizontalLayout.addWidget(self.buttonsFrame)
        self.verticalLayout.addWidget(self.titleBar)
        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        self.stackedWidget.setStyleSheet("background-color: none;")
        self.stackedWidget.setObjectName("stackedWidget")
        self.page = QtWidgets.QWidget()
        self.page.setObjectName("page")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.page)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.mainFrame = QtWidgets.QFrame(self.page)
        self.mainFrame.setStyleSheet("background-color: none;")
        self.mainFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.mainFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.mainFrame.setObjectName("mainFrame")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.mainFrame)
        self.horizontalLayout_4.setContentsMargins(10, 10, 0, 0)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.dataFrame = QtWidgets.QFrame(self.mainFrame)
        self.dataFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.dataFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.dataFrame.setObjectName("dataFrame")
        self.formLayout = QtWidgets.QFormLayout(self.dataFrame)
        self.formLayout.setContentsMargins(10, 10, 0, 10)
        self.formLayout.setObjectName("formLayout")
        self.startDataEndLabel = QtWidgets.QLabel(self.dataFrame)
        self.startDataEndLabel.setStyleSheet("color: rgb(255, 255, 255);")
        self.startDataEndLabel.setObjectName("startDataEndLabel")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.startDataEndLabel)
        self.startDateTimeEdit = QtWidgets.QDateTimeEdit(self.dataFrame)
        self.startDateTimeEdit.setDateTime(QtCore.QDateTime(QtCore.QDate(2022, 1, 3), QtCore.QTime(10, 0, 0)))
        self.startDateTimeEdit.setObjectName("startDateTimeEdit")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.startDateTimeEdit)
        self.endDateTimeLabel = QtWidgets.QLabel(self.dataFrame)
        self.endDateTimeLabel.setStyleSheet("color: rgb(255, 255, 255);")
        self.endDateTimeLabel.setObjectName("endDateTimeLabel")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.endDateTimeLabel)
        self.endDateTimeEdit = QtWidgets.QDateTimeEdit(self.dataFrame)
        self.endDateTimeEdit.setDateTime(QtCore.QDateTime(QtCore.QDate(2022, 1, 4), QtCore.QTime(10, 0, 0)))
        self.endDateTimeEdit.setObjectName("endDateTimeEdit")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.endDateTimeEdit)
        self.symbolLabel = QtWidgets.QLabel(self.dataFrame)
        self.symbolLabel.setStyleSheet("color: rgb(255, 255, 255);")
        self.symbolLabel.setObjectName("symbolLabel")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.symbolLabel)
        self.symbolTextEdit = QtWidgets.QTextEdit(self.dataFrame)
        self.symbolTextEdit.setMaximumSize(QtCore.QSize(16777215, 30))
        self.symbolTextEdit.setStyleSheet("")
        self.symbolTextEdit.setObjectName("symbolTextEdit")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.symbolTextEdit)
        self.label = QtWidgets.QLabel(self.dataFrame)
        self.label.setStyleSheet("color: rgb(255, 255, 255);")
        self.label.setObjectName("label")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.label)
        self.frame = QtWidgets.QFrame(self.dataFrame)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.openButton = QtWidgets.QRadioButton(self.frame)
        self.openButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.openButton.setObjectName("openButton")
        self.openButton.setStyleSheet("color: rgb(255, 255, 255);")
        self.horizontalLayout_7.addWidget(self.openButton)
        self.lowButton = QtWidgets.QRadioButton(self.frame)
        self.lowButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.lowButton.setObjectName("lowButton")
        self.lowButton.setStyleSheet("color: rgb(255, 255, 255);")
        self.horizontalLayout_7.addWidget(self.lowButton)
        self.highButton = QtWidgets.QRadioButton(self.frame)
        self.highButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.highButton.setObjectName("highButton")
        self.highButton.setStyleSheet("color: rgb(255, 255, 255);")
        self.horizontalLayout_7.addWidget(self.highButton)
        self.closeButton = QtWidgets.QRadioButton(self.frame)
        self.closeButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.closeButton.setObjectName("closeButton")
        self.closeButton.setStyleSheet("color: rgb(255, 255, 255);")
        self.horizontalLayout_7.addWidget(self.closeButton)
        self.formLayout.setWidget(7, QtWidgets.QFormLayout.LabelRole, self.frame)
        spacerItem = QtWidgets.QSpacerItem(100, 300, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout.setItem(8, QtWidgets.QFormLayout.LabelRole, spacerItem)
        self.horizontalLayout_4.addWidget(self.dataFrame)
        self.buttonFrame = QtWidgets.QFrame(self.mainFrame)
        self.buttonFrame.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.buttonFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.buttonFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.buttonFrame.setObjectName("buttonFrame")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.buttonFrame)
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.plotButton = QtWidgets.QPushButton(self.buttonFrame)
        self.plotButton.setMinimumSize(QtCore.QSize(50, 30))
        self.plotButton.setMaximumSize(QtCore.QSize(200, 30))
        self.plotButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.plotButton.setWhatsThis("")
        self.plotButton.setStyleSheet("QPushButton {\n"
"border: none;\n"
"border-radius: 8px;\n"
"background-color: rgb(57, 109, 57);\n"
"color: rgb(255, 255, 255);\n"
"}\n"
"QPushButton:hover {\n"
"background-color: rgb(57, 109, 57,150);\n"
"}")
        self.plotButton.setShortcut("")
        self.plotButton.setObjectName("plotButton")
        self.horizontalLayout_5.addWidget(self.plotButton)
        self.aiButton = QtWidgets.QPushButton(self.buttonFrame)
        self.aiButton.setMinimumSize(QtCore.QSize(50, 30))
        self.aiButton.setMaximumSize(QtCore.QSize(200, 30))
        self.aiButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.aiButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.aiButton.setStyleSheet("QPushButton {\n"
"border: none;\n"
"border-radius: 8px;\n"
"background-color: rgb(162, 162, 162);\n"
"color: rgb(255, 255, 255);\n"
"}\n"
"QPushButton:hover {\n"
"background-color: rgb(162, 162, 162,150);\n"
"}")
        self.aiButton.setObjectName("aiButton")
        self.horizontalLayout_5.addWidget(self.aiButton)
        self.horizontalLayout_4.addWidget(self.buttonFrame)
        self.horizontalLayout_4.setStretch(0, 1)
        self.horizontalLayout_4.setStretch(1, 1)
        self.verticalLayout_3.addWidget(self.mainFrame)
        self.stackedWidget.addWidget(self.page)
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setObjectName("page_2")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.page_2)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.plotFrame = QtWidgets.QFrame(self.page_2)
        self.plotFrame.setStyleSheet("background-color: none;")
        self.plotFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.plotFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.plotFrame.setObjectName("plotFrame")
        self.verticalLayout_5.addWidget(self.plotFrame)
        self.backButton = QtWidgets.QPushButton(self.page_2)
        self.backButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.backButton.setWhatsThis("")
        self.backButton.setStyleSheet("QPushButton {\n"
"border: none;\n"
"border-radius: 8px;\n"
"background-color: rgb(190, 190, 190);\n"
"color: rgb(255, 255, 255);\n"
"}\n"
"QPushButton:hover {\n"
"background-color: rgb(190, 190, 190,150);\n"
"}\n"
"")
        self.backButton.setObjectName("backButton")
        self.verticalLayout_5.addWidget(self.backButton)
        self.stackedWidget.addWidget(self.page_2)
        self.verticalLayout.addWidget(self.stackedWidget)
        self.creditsBar = QtWidgets.QFrame(self.centralwidget)
        self.creditsBar.setMaximumSize(QtCore.QSize(16777215, 30))
        self.creditsBar.setStyleSheet("background-color: none;")
        self.creditsBar.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.creditsBar.setFrameShadow(QtWidgets.QFrame.Raised)
        self.creditsBar.setObjectName("creditsBar")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.creditsBar)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.creditsLabel = QtWidgets.QLabel(self.creditsBar)
        self.creditsLabel.setMinimumSize(QtCore.QSize(0, 30))
        self.creditsLabel.setMaximumSize(QtCore.QSize(16777215, 30))
        font = QtGui.QFont()
        font.setFamily("MV Boli")
        font.setPointSize(12)
        self.creditsLabel.setFont(font)
        self.creditsLabel.setStyleSheet("color: white;")
        self.creditsLabel.setText("By Grzegorz Kurenda")
        self.creditsLabel.setObjectName("creditsLabel")
        self.horizontalLayout_3.addWidget(self.creditsLabel)
        self.verticalLayout.addWidget(self.creditsBar)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.stackedWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        self.minimizeButton.setToolTip(_translate("MainWindow", "Minimalize"))
        self.maximizeButton.setToolTip(_translate("MainWindow", "Maximalize"))
        self.exitButton.setToolTip(_translate("MainWindow", "Close"))
        self.startDataEndLabel.setText(_translate("MainWindow", "Data początkowa"))
        self.endDateTimeLabel.setText(_translate("MainWindow", "Data końcowa"))
        self.symbolLabel.setText(_translate("MainWindow", "Symbol"))
        self.symbolTextEdit.setPlaceholderText(_translate("MainWindow", "np. EURCAD."))
        self.label.setText(_translate("MainWindow", "Typ danych"))
        self.openButton.setText(_translate("MainWindow", "Open"))
        self.lowButton.setText(_translate("MainWindow", "Low"))
        self.highButton.setText(_translate("MainWindow", "High"))
        self.closeButton.setText(_translate("MainWindow", "Close"))
        self.plotButton.setText(_translate("MainWindow", "Wyświetl dane handlowe"))
        self.aiButton.setText(_translate("MainWindow", "Przykładowa Sztuczna Inteligencja"))
        self.backButton.setText(_translate("MainWindow", "Powrót"))



