from PyQt5.QtWidgets import QMessageBox


class BasicPopup:

    @staticmethod
    def createPopup(string):
        popup = QMessageBox()
        popup.setIcon(QMessageBox.Critical)
        popup.setText(string)
        popup.setWindowTitle("Error")
        popup.exec_()
