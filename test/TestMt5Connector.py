import unittest

import numpy
import pytz
from datetime import datetime, timedelta
from logic.Mt5Connector import Mt5Connector
import MetaTrader5


class TestMt5Connector(unittest.TestCase):

    def test_with_1000001_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=1000001)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_MN1

    def test_with_1000000_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=1000000)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_W1

    def test_with_100001_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=100001)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_W1

    def test_with_100000_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=100000)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_D1

    def test_with_10001_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=10001)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_D1

    def test_with_10000_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=10000)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_H1

    def test_with_1001_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=1001)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_H1

    def test_with_1000_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=1000)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_M5

    def test_with_10_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=10)
        d2 = datetime.now(timezone)
        assert Mt5Connector.createInterval(self, d1, d2) == MetaTrader5.TIMEFRAME_M5

    def test_with_9_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=9)
        d2 = datetime.now(timezone)
        with self.assertRaises(Exception) as context:
            Mt5Connector.createInterval(self, d1, d2)

    def test_with_0_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=0)
        d2 = datetime.now(timezone)
        with self.assertRaises(Exception) as context:
            Mt5Connector.createInterval(self, d1, d2)

    def test_with_minus_1_minutes_createInterval(self):
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(minutes=-1)
        d2 = datetime.now(timezone)
        with self.assertRaises(Exception) as context:
            Mt5Connector.createInterval(self, d1, d2)

    def test_createDataFrame(self):
        mt5 = Mt5Connector()
        data = Mt5Connector.getDataForSI(mt5, 20)
        dataFrame = mt5.createDataFrame(data, "Close")
        assert type(dataFrame) == list

    def test_no_type_createDataFrame(self):
        mt5 = Mt5Connector()
        data = Mt5Connector.getDataForSI(mt5, 20)
        with self.assertRaises(Exception) as context:
            dataFrame = mt5.createDataFrame(data, None)

    def test_no_data_createDataFrame(self):
        mt5 = Mt5Connector()
        with self.assertRaises(Exception) as context:
            dataFrame = mt5.createDataFrame(None, "Close")

    def test_getTicksRange(self):
        mt5 = Mt5Connector()
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(days=1)
        d2 = datetime.now(timezone)
        data = mt5.getTicksRange("EURCAD.", d1, d2)
        assert type(data) == numpy.ndarray

    def test_no_symbol_getTicksRange(self):
        mt5 = Mt5Connector()
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(days=1)
        d2 = datetime.now(timezone)
        with self.assertRaises(Exception) as context:
            data = mt5.getTicksRange(None, d1, d2)

    def test_with_bad_symbol_getTicksRange(self):
        mt5 = Mt5Connector()
        timezone = pytz.timezone("Etc/UTC")
        d1 = datetime.now(timezone) - timedelta(days=1)
        d2 = datetime.now(timezone)
        with self.assertRaises(Exception) as context:
            data = mt5.getTicksRange("PLN_To_EURO", d1, d2)