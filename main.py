
from PyQt5 import QtWidgets
from ui.Window import Window

if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    ui = Window()
    sys.exit(app.exec_())
